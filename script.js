// #### Технічні вимоги:
// - Надіслати AJAX запит на адресу `https://ajax.test-danit.com/api/swapi/films` та отримати список усіх фільмів серії `Зоряні війни`
// - Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості `characters`.
// - Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля `episodeId`, `name`, `openingCrawl`).
// - Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

const API = "https://ajax.test-danit.com/api/swapi/films";
const sendRequest = async (url) => {
    const response = await fetch(url);
    const result = await response.json();
    return result;
}

const getCharacters = (id, characters ) => {

const charactersList =  Promise.all(characters.map((elem) => sendRequest(elem)))
charactersList.then((data) => {
    console.log(data);
    data.forEach(({name}) => {
        document.getElementById(id).innerHTML += `<span> ${name}; </span>`
    })
})

}

const renderFilms = (url) => {
    sendRequest(url)
        .then((data) => {
            console.log(data);
            const list = document.createElement("ul")
     
            data.forEach(({ id, episodeId, name, openingCrawl, characters }) => {
                // const li = document.createElement('li');
                list.insertAdjacentHTML('afterbegin', `
            <li> <h4> Номер эпизода: ${episodeId}</h4>
            <h3 class="title"> Название фильма: ${name} </h3>
            <p id="${id}"></p>
    <p> Короткое содержание: ${openingCrawl}</p></li>
            `)
            // list.append(li);
              getCharacters(id, characters);
    
        
                        
            })
           
           document.body.append(list)
        });

      
            
//     .catch ((error) => {
//     console.log(error.message);
// })
}
renderFilms(API)
